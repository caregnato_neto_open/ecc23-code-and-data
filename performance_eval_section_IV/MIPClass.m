classdef MIPClass < handle

    properties
        nx = [];
        nu = [];
        number_tar = [];
        number_obs = [];
        number_robots = [];
        number_obs_sides = [];
        number_body_sides = [];
        number_angles = [];
        N = [];
        BigM = [];
        opt_vars = [];
        constraints = [];
        index = [];
        cost = []
        control_weight = [];
        beta = [];
        
    end
    
    methods
        function Construct(MIP,agents,poly,n_agents)
            
            % Auxiliary vars
            MIP.nx = size(agents{1}.dyn.a,1);
            MIP.nu = size(agents{1}.dyn.b,2);
          %  MIP.number_robots = length(agents);
            MIP.number_robots = n_agents;
            MIP.number_obs = length(poly.obs);
%            MIP.number_tar = length(poly.tar);
            MIP.number_obs_sides = size(poly.obs{1}.P,1);
            
            MIP.number_body_sides = size(poly.agent_body{1}.P,1);
            
            
            
            
            
            % Optimization parameters
            MIP.N = 0;
            MIP.BigM = 100;
            MIP.control_weight = 10;
            n_los_points = 20;
            MIP.beta = [1/(n_los_points+1):1/(n_los_points+1):1-(1/(n_los_points+1))];
            
            % Real-valued optimization variables
            MIP.opt_vars.X = sdpvar((MIP.N+1)*MIP.number_robots*MIP.nx,1);
            MIP.opt_vars.U = sdpvar(MIP.N*MIP.nu*MIP.number_robots,1);
            
            %             MIP.opt_vars.lin_vel = sdpvar((MIP.N+1)*MIP.number_robots,1);
            %             MIP.opt_vars.acc = sdpvar((MIP.N+1)*MIP.number_robots,1);
            %             MIP.opt_vars.abs_acc = sdpvar((MIP.N+1)*MIP.number_robots,1);
            
            
            % Binary variables
            MIP.opt_vars.B_hor = binvar(MIP.N+1,1);
%            MIP.opt_vars.B_tar = binvar((MIP.N+1)*MIP.number_tar*MIP.number_robots,1);
            MIP.opt_vars.B_obs = binvar((MIP.N+1)*MIP.number_robots*MIP.number_obs*MIP.number_obs_sides,1);
            MIP.opt_vars.B_col = binvar((MIP.N+1)*sum(1:MIP.number_robots-1)*MIP.number_body_sides,1);
            MIP.opt_vars.B_con = binvar((MIP.N+1)*sum(1:MIP.number_robots-1),1);
            MIP.opt_vars.B_los = binvar(MIP.number_obs*length(MIP.beta)*(MIP.N+1)*sum(1:MIP.number_robots-1),1);
           
            MIP.opt_vars.B_old_los = binvar(MIP.number_obs_sides*(length(MIP.beta)-1)*(MIP.N+1)*MIP.number_obs*sum(1:MIP.number_robots-1),1);
            
            
            % Auxiliary optimization variables
            %MIP.opt_vars.x0 = sdpvar(MIP.number_robots*MIP.nx,1);
            
        end
        
        function BuildOptVarIndexes(MIP)
            
            counter = 0;
            counter_u = 0;
            counter_obs = 0;
            counter_col = 0;
            counter_tar = 0;
            counter_con = 0;
            counter_los = 0;
            counter_los_old = 0;
            
            
            for i = 1 : MIP.number_robots
                for k = 1 : MIP.N+1
                    
                    counter = counter + 1;
                    % linear velocity (nu)
                    MIP.index.lin_vel(k,i) = counter;
                    
                    
                    % old los const
                    if i <= MIP.number_robots-1
                        for j = i + 1 : MIP.number_robots
                            for n = 1 : MIP.number_obs
                                for z = 1 : length(MIP.beta)-1
                                    for g = 1 : MIP.number_obs_sides
                                        counter_los_old = counter_los_old + 1;
                                        MIP.index.B_old_los(g,z,n,k,i,j) = counter_los_old;
                                    end
                                end
                            end
                            
                        end
                    end
                    
                    % state
                    MIP.index.x(:,k,i) = (counter-1)*MIP.nx+1:counter*MIP.nx;
                    
                    % obs bin
                    for j = 1 : MIP.number_obs
                        for n = 1 : MIP.number_obs_sides
                            counter_obs =  counter_obs + 1;
                            MIP.index.B_obs(n,k,i,j) = counter_obs; %...
                            %(counter-1)*ones(MIP.number_obs_sides,1)+1:counter*ones(MIP.number_obs_sides,1);
                        end
                    end
                    
                    % col bin
                    if i <= MIP.number_robots-1
                        for j = i + 1 : MIP.number_robots
                            for m = 1 : MIP.number_body_sides
                                counter_col = counter_col + 1;
                                MIP.index.B_col(m,k,i,j) = counter_col;
                                %= (counter-1)*MIP.number_body_sides+1:counter*MIP.number_body_sides;
                            end
                        end
                    end
                    
                    % con bin
                    if i <= MIP.number_robots-1
                        for j = i + 1 : MIP.number_robots
                            counter_con = counter_con + 1;
                            MIP.index.B_con(k,i,j) = counter_con;
                        end
                    end
                    
%                     % b_los
%                     if i <= MIP.number_robots-1
%                         for j = i + 1 : MIP.number_robots
%                             for z = 1 : length(MIP.beta)
%                                 counter_los = counter_los + 1;
%                                 MIP.index.B_los(j,i,z,k) =  counter_los;
%                             end
%                         end
%                     end

                 % b_los
                    if i <= MIP.number_robots-1
                        for j = i + 1 : MIP.number_robots
                            for z = 1 : length(MIP.beta)
                                for w = 1 : MIP.number_obs
                                counter_los = counter_los + 1;
                                MIP.index.B_los(j,i,z,k,w) =  counter_los;
                                end
                            end
                        end
                    end
                    
                end
                
                % input (outside time loop to N+1)
                for z = 1 : MIP.N
                    for ww = 1 : MIP.nu
                        counter_u = counter_u + 1;
                        MIP.index.u(ww,z,i) = counter_u; % input
                    end
                end
                
            end
            
            
            
            
            % target bin
%             for k = 1 : MIP.N+1
%                 for i = 1 : MIP.number_robots
%                     for j = 1 : MIP.number_tar
%                         counter_tar = counter_tar + 1;
%                         MIP.index.B_tar(k,j,i) = counter_tar;
%                     end
%                 end
%             end
            
            
        end
        
        function BuildLinearDynamicConstraints(MIP,agents)
            A = agents{1}.dyn.a;
            B = agents{1}.dyn.b;
            
            MIP.constraints.dyn = [];
            MIP.constraints.initial_condition = [];
            for i = 1 : MIP.number_robots
                
                % Initial conditions
                MIP.constraints.initial_condition = [MIP.constraints.initial_condition,
                    MIP.opt_vars.X(MIP.index.x(:,1,i)) == agents{i}.x0,
                    ];
             
                
                % Dynamics
%                 for k = 1 : MIP.N
%                     
%                     MIP.constraints.dyn = [MIP.constraints.dyn,
%                         MIP.opt_vars.X(MIP.index.x(:,k+1,i)) <= A*MIP.opt_vars.X(MIP.index.x(:,k,i))+B*MIP.opt_vars.U(MIP.index.u(:,k,i))];% + ones(MIP.nx,1)*sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM];
%                     
%                     MIP.constraints.dyn = [MIP.constraints.dyn,
%                         -MIP.opt_vars.X(MIP.index.x(:,k+1,i)) <= -A*MIP.opt_vars.X(MIP.index.x(:,k,i))-B*MIP.opt_vars.U(MIP.index.u(:,k,i))];% + ones(MIP.nx,1)*sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM];
%                     
%                 end
            end
        end
        
        function BuildObstacleAvoidanceConstraints(MIP,poly)
            
            MIP.constraints.obstacles = [];
            MIP.constraints.obstacles_binary = [];
            MIP.constraints.obstacles_corner_cutting = [];
            for i = 1 : MIP.number_robots
                for k = 1 : MIP.N+1
                    
                    % aux
                    pos = [MIP.opt_vars.X(MIP.index.x(1,k,i));
                        MIP.opt_vars.X(MIP.index.x(3,k,i))];
                    
                    for j = 1 : MIP.number_obs
                        MIP.number_obs_sides = length(poly.obs{j}.P);
                        B_obs = MIP.opt_vars.B_obs(MIP.index.B_obs(:,k,i,j));
                        
                        % Obs entry constraint
                        MIP.constraints.obstacles = [MIP.constraints.obstacles,
                            -poly.obs{j}.P*pos <= -poly.obs{j}.q+(ones(MIP.number_obs_sides,1)-B_obs)*MIP.BigM];%+ones(MIP.number_obs_sides,1)*sum(MIP.opt_vars.B_hor(1:k-1))*MIP.BigM];
                        
                        % obs binary constraint
                        MIP.constraints.obstacles_binary = [MIP.constraints.obstacles_binary,sum(B_obs) >= 1];
                           
                    end
                end
            end
            
            
        end
        
        function BuildOldIntermediaryPointsLOSConstraint(MIP,poly)
            
            
            MIP.constraints.old_los = [];
            MIP.constraints.bin_old_los = [];
            MIP.constraints.los_corner_cut = [];
            
            B_con = MIP.opt_vars.B_con;
           B_obs = MIP.opt_vars.B_obs;
            B_hor = MIP.opt_vars.B_hor;
            
            
            
            % robots
            
            for j = 1 : MIP.number_robots-1
                for i = j + 1 : MIP.number_robots
                    for k = 1 : MIP.N + 1
                        
                        % Gets position Variable at k-th time step for
                        % a pair of agent
                        pos_j = [MIP.opt_vars.X(MIP.index.x(1,k,j));MIP.opt_vars.X(MIP.index.x(3,k,j))];
                        pos_i = [MIP.opt_vars.X(MIP.index.x(1,k,i));MIP.opt_vars.X(MIP.index.x(3,k,i))];
                        
                        for n = 1 : MIP.number_obs
                            
                            for z = 1 : length(MIP.beta)-1
                                
                                % build LOS samples
                                sample = pos_j*(1-MIP.beta(z)) + MIP.beta(z)*pos_i;
                                
                                % Collision avoidance constraint for the % z-th sample
                                MIP.constraints.old_los = [MIP.constraints.old_los,
                                    -poly.obs{n}.P*sample <= -poly.obs{n}.q+MIP.BigM*(ones(MIP.number_obs_sides,1)-MIP.opt_vars.B_old_los(MIP.index.B_old_los(:,z,n,k,j,i)))];
                                
                                %  MIP.constraints.bin_old_los = [MIP.constraints.bin_old_los,...
                                %     sum(MIP.opt_vars.B_old_los(MIP.index.B_old_los(:,z,n,k,j,i))) == B_con(MIP.index.B_con(k,i,j))];
                                
                                MIP.constraints.bin_old_los = [MIP.constraints.bin_old_los,...
                                    B_con(MIP.index.B_con(k,j,i)) <= sum(MIP.opt_vars.B_old_los(MIP.index.B_old_los(:,z,n,k,j,i)))];
                                %   B_con(MIP.index.B_con(k,j,i)) <= sum(MIP.opt_vars.B_old_los(MIP.index.B_old_los(:,z,n,k,j,i)))];
                                
                                
                                % Now me must implement the corner cutting
                                % constraint. For that, the binary of the
                                % z-th sample must be a solution for the
                                % obstacle avoidance problem of the next
                                % sample
                                nextSample = pos_j*(1-MIP.beta(z+1)) + pos_i*MIP.beta(z+1);
                                
                                %
                                MIP.constraints.los_corner_cut = [MIP.constraints.los_corner_cut,...
                                    -poly.obs{n}.P*nextSample <= -poly.obs{n}.q+MIP.BigM*(ones(MIP.number_obs_sides,1)-MIP.opt_vars.B_old_los(MIP.index.B_old_los(:,z,n,k,j,i)))];%+MIP.BigM*ones(MIP.number_obs_sides,1)*sum(B_hor(1:k-1))];
                            end % sample loop
                            
                            
                            % The corner cutting inside the sample loop is
                            % between samples. Here I apply the constraint
                            % to the pairs (y_1,sample(1)) and
                            % (sample(end),y_2)
                            
                            %  if n ~= 4
                            firstSample = pos_j*(1-MIP.beta(1)) + MIP.beta(1)*pos_i;
                            lastSample = pos_j*(1-MIP.beta(end)) + MIP.beta(end)*pos_i;
                            
         
                            con_bin_sum = B_con(MIP.index.B_con(k,j,i));
                            MIP.constraints.los_corner_cut = [MIP.constraints.los_corner_cut,...
                                -poly.obs{n}.P*firstSample <= -poly.obs{n}.q+MIP.BigM*(ones(MIP.number_obs_sides,1)-B_obs(MIP.index.B_obs(:,k,j,n)))+ones(MIP.number_obs_sides,1)*(1-con_bin_sum)*MIP.BigM];%+MIP.BigM*ones(MIP.number_obs_sides,1)*sum(B_hor(1:k-1))];
                            MIP.constraints.los_corner_cut = [MIP.constraints.los_corner_cut,...
                                -poly.obs{n}.P*lastSample <= -poly.obs{n}.q+MIP.BigM*(ones(MIP.number_obs_sides,1)-B_obs(MIP.index.B_obs(:,k,i,n)))+ones(MIP.number_obs_sides,1)*(1-con_bin_sum)*MIP.BigM];%+MIP.BigM*ones(MIP.number_obs_sides,1)*sum(B_hor(1:k-1))];
                            %
                            %     end
                            
                            %       opt.sampleCornerCuttingConst = [opt.sampleCornerCuttingConst,-env.P_obs{n}*lastSample <= -env.q_obs{n}...
                            %     + opt.BigM*(ones(env.Nos,1)-opt.B_obs(opt.B_obsIndex(:,n,k,i)))+...
                            %   opt.BigM*ones(env.Nos,1)*sum(opt.B_hor(1:k-1))];
                            %
                            
                            
                        end %  obstacle loop
                    end %  time loop
                end  % agent j loop
            end % agent i loop
            
            
            
        end
        
        function BuildIntermediaryPointsLOSConstraint(MIP,poly)
            
            B_obs = MIP.opt_vars.B_obs;
            B_los = MIP.opt_vars.B_los;
            B_con = MIP.opt_vars.B_con;
            
            MIP.constraints.los = [];
            MIP.constraints.los_bin = [];
           
          %  MIP.constraints.obstacles = [];
            for k = 1 : MIP.N + 1
                
                for i = 1 : MIP.number_robots - 1
                    pos_i = [MIP.opt_vars.X(MIP.index.x(1,k,i));
                        MIP.opt_vars.X(MIP.index.x(3,k,i))];
                    
                    for j = i + 1 : MIP.number_robots
                        pos_j = [MIP.opt_vars.X(MIP.index.x(1,k,j));
                            MIP.opt_vars.X(MIP.index.x(3,k,j))];
                        
                        for n = 1 : MIP.number_obs
                            MIP.number_obs_sides = length(poly.obs{n}.P);
                            
                            for z = 1 : length(MIP.beta)
                                sample_ij = pos_i*(1-MIP.beta(z)) + pos_j*MIP.beta(z);
                                
                                MIP.constraints.los = [MIP.constraints.los,
                                    -poly.obs{n}.P*sample_ij <= -poly.obs{n}.q...
                                    + (ones(MIP.number_obs_sides,1)-B_obs(MIP.index.B_obs(:,k,i,n)))*MIP.BigM...
                                    + ones(MIP.number_obs_sides,1)*(1-B_los(MIP.index.B_los(j,i,z,k,n)))*MIP.BigM];
                                
                                MIP.constraints.los = [MIP.constraints.los,
                                    -poly.obs{n}.P*sample_ij <= -poly.obs{n}.q...
                                    + (ones(MIP.number_obs_sides,1)-B_obs(MIP.index.B_obs(:,k,j,n)))*MIP.BigM...
                                    + ones(MIP.number_obs_sides,1)*(1-B_los(MIP.index.B_los(j,i,z,k,n)))*MIP.BigM];
                                
                            end
                            MIP.constraints.los_bin = [MIP.constraints.los_bin,
                                B_con(MIP.index.B_con(k,i,j)) <= sum(B_los(MIP.index.B_los(j,i,:,k,n)))];
                        end
                    end
                end
            end
        end
        
        function BuildConnectivityRegionConstraint(MIP,poly)
            
            B_con = MIP.opt_vars.B_con;
            
            MIP.constraints.conn_region = [];
            for k = 1 : MIP.N + 1
                for i = 1 : MIP.number_robots - 1
                    pos_i = [MIP.opt_vars.X(MIP.index.x(1,k,i));
                        MIP.opt_vars.X(MIP.index.x(3,k,i))];
                    
                    
                    for j = i + 1 : MIP.number_robots
                        pos_j = [MIP.opt_vars.X(MIP.index.x(1,k,j));
                            MIP.opt_vars.X(MIP.index.x(3,k,j))];
                        
                        
                        rel_pos = pos_i - pos_j;
                        MIP.constraints.conn_region = [MIP.constraints.conn_region,...
                            poly.conn_region.P*rel_pos <= poly.conn_region.q + ones(8,1)*(1-B_con(MIP.index.B_con(k,i,j)))*MIP.BigM];
                        
                        
                    end
                end
            end
        end
        
        function BuildCost(MIP)
            %              aux = [];
            %             for i = 1 : MIP.number_robots
            %                 aux = [aux, transpose(MIP.opt_vars.acc(MIP.index.lin_vel(:,i)))*MIP.opt_vars.acc(MIP.index.lin_vel(:,i))];
            %             end
            
            %MIP.cost =  MIP.opt_vars.U'*MIP.opt_vars.U;% + MIP.control_weight*sum(aux);
            B_con = MIP.opt_vars.B_con;
            MIP.cost = -sum(B_con);
        end
        
    end % end methods
end % end class

