clc; clearvars; close all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% New Line-of-Sight constraint evaluation as discussed in section IV of:
%
% "A Novel Line of Sight Constraint for Mixed-Integer Programming
%  Models with Applications to Multi-Agent Motion Planning"
%
% Angelo Caregnato-Neto, Marcos Omena de Albuquerque Maximo, and Rubens
% Junqueira Magalhães Afonso (Instituto Tecnológico de Aeronáutica)
%
% Presented in the 21st European Control Conference, Bucharest, Romania.
%
% This code is far from optimized as its purpose is merely reproducibility
%
% It is compatible with MATLAB 2020b and requires the YALMIP and MPT 
% toolboxes and some MIP solver. Here it is hardcoded for gurobi.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Evaluation initialization
n_trials = 5;
sol_old = cell(n_trials,1);
sol_new = cell(n_trials,1);
rng_scen = cell(n_trials,1);
data = cell(n_trials,1);

% These are not the parameters in the paper
params.number_obs_sides = 4;
params.number_obstacles = 3;
params.number_agents = 3;

i = 1;
while i <= n_trials
    display("Trial: " + i);
    yalmip('clear')
    
    % Builds random scenario
    scenario = scenarioClass;
    rng_scen{i} = GenRngScenObsSide(params);
    
    % Guarantees that the number of sides in each obstacle is really as
    % intended. Sometimes 
    proceed = true;
    for g = 1 : params.number_obstacles 
        if length(rng_scen{i}.polytopes.obs{g}.P) ~= params.number_obs_sides
            proceed = false;
            break
        end  
    end
    
   if proceed
    scenario.Construct(rng_scen{i});
    disp("Random scenario succesfully generated");
    
    % Build problem
    timer = tic;
    model = build_optimization(scenario);
    build_time = toc(timer)
    
    % Solve with new constraint
    disp("Starting optimization - new constraint");
    isNewConst = 1;
    sol_new{i} = solve_model(model,isNewConst);
  
    disp("Salving data");
    analysis = AnalysisClass;
    analysis.Construct(sol_new{i}.MIP,scenario);
    data{i}.new = analysis.BuildFileForStorage();
    data{i}.new.opt_time = sol_new{i}.opt_time;
    data{i}.new.cost = sum(value(sol_new{i}.MIP.opt_vars.B_con));
    
     % Solve with old constraint
    disp("Starting optimization old constraint");
    isNewConst = 0;
    sol_old{i} = solve_model(model,isNewConst);
    
    analysis = AnalysisClass;
    analysis.Construct(sol_old{i}.MIP,scenario);
    data{i}.old = analysis.BuildFileForStorage();
    data{i}.old.opt_time = sol_old{i}.opt_time;
    data{i}.old.cost = sum(value(sol_old{i}.MIP.opt_vars.B_con));
    
    i = i + 1;
 
   else
      i = i - 1;
   end

end
%% plots
close all;

figure; hold; grid;
for i = 1 : n_trials
    plot(data{i}.new.opt_time,data{i}.old.opt_time,'x');
    lim_axis = max([data{i}.new.opt_time,data{i}.new.opt_time,data{i}.old.opt_time,data{i}.old.opt_time]);
    
    plot([0:0.01:lim_axis],[0:0.01:lim_axis],'k--');
    xlabel("Solve Time New Constraint (s)"); ylabel("Solve Time Old Constraint (s)");
end



% Plot scenario and LOS
n_scen = 1;
scenario = scenarioClass;
scenario.Construct(rng_scen{n_scen});
fig = scenario.Plot(3);


ind = data{n_scen}.new.index;
for ag = 1 : params.number_agents-1
    for aj = ag+1 : params.number_agents
      
        if  data{n_scen}.new.B_con(ind.B_con(1,ag,aj))
            plot([data{n_scen}.new.traj(1,1,ag),data{n_scen}.new.traj(1,1,aj)],[data{n_scen}.new.traj(2,1,ag),data{n_scen}.new.traj(2,1,aj)],'r--');
        end
    end
end
exportgraphics(fig,'example.pdf','ContentType','vector') 
