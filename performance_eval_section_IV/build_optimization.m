function mip_model = build_optimization(scenario)


mip_model = MIPClass;
mip_model.Construct(scenario.agents,scenario.polytopes,scenario.number_agents);
mip_model.BuildOptVarIndexes();


% Contraints
 mip_model.BuildLinearDynamicConstraints(scenario.agents);
 mip_model.BuildObstacleAvoidanceConstraints(scenario.polytopes);

% New los
    mip_model.BuildIntermediaryPointsLOSConstraint(scenario.polytopes)

% Old los
     mip_model.BuildOldIntermediaryPointsLOSConstraint(scenario.polytopes);

% Cost
mip_model.BuildCost(); 





end

