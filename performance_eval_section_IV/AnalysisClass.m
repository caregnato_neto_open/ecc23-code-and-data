classdef AnalysisClass < handle
    %MIPCLASS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        mip = [];
        scenario = [];
        N_opt = [];
        traj = [];
        vel = [];
        acc = [];
        colors = [];
        agents = [];
    end
    
    methods
        function Construct(obj,mip,scenario)
            
            obj.mip = mip;
            obj.scenario = scenario;
            obj.N_opt = find(value(mip.opt_vars.B_hor) == 1);
            obj.agents = scenario.agents;

            
            % pos
            for i = 1 : mip.number_robots
                for k = 1 : 1
                    obj.traj(:,k,i) = [value(mip.opt_vars.X(mip.index.x(1,k,i)));value(mip.opt_vars.X(mip.index.x(3,k,i)))];
                end
            end
            
            
        end
        
        %%%%%%%%%
        function PlotTrajectories(obj)
            
            obj.scenario.Plot(1);
            for i = 1 : data.number_robots
                plot(obj.traj(1,1:obj.N_opt,i),obj.traj(2,1:obj.N_opt,i));
            end
            xlabel('r_x'); ylabel('r_y');
            
        end
        
        %%%%%%%%%
        function BuildColors(obj,data)
            % Pre-allocation
            % Colors
            obj.colors = zeros(data.number_robots,3);
            
            % Assign pallet of colors to variable
            c = jet;
            
            % First plt.Data.agent is always assing to the color in the first index
            obj.colors(1,:) = c(1,:);
            
                        % Assign colors to the remaining
            for i = 2 : data.number_robots
                obj.colors(i,:) = c(i*floor(length(c)/data.number_robots),:);
            end
        end
        
        %%%%%%%%%
        function PlotVelocities(obj,data)
            
            figure();  xlabel('v_x'); ylabel('v_y');
            for i=1:24
                plot(Polyhedron(data.polytopes.orient{i}.P,data.polytopes.orient{i}.q)); hold on; 
            end

            for j = 1 : data.number_robots
                for k = 1 : data.N_opt 
                    orientation(k,j) = (pi/12)*(find(round(data.B_orient(data.index.B_orient(k,j,:),1))==1)-1);
                    plot(value(data.lin_vel(data.index.lin_vel(k,j)))*cos(orientation(k,j)),value(data.lin_vel(data.index.lin_vel(k,j)))*sin(orientation(k,j)),'o');
                end
           % plot(data.vel(1,1:data.N_opt,j),data.vel(2,1:data.N_opt,j),'o');
           
            end
            
           
            
        end
        
        %%%%%%%%%
        function PlotScenario(obj,timestep,data)
            
                       % Colors
            data.agents{1}.color = 'blue'; % base
            data.agents{end}.color = 'red'; % leader
            for i = 2 : data.number_robots-1; data.agents{i}.color = 'green'; end % followers
            
            
            % Plot operational region
            figure(timestep); hold on;
            field = Polyhedron(data.polytopes.op_region.P,data.polytopes.op_region.q);
            field.plot('color','white','alpha',0.2)
            % plot(Polyhedron(obj.polytopes.op_region.P,obj.polytopes.op_region.q),'color','white','alpha',1);
            
            % Plot obstacles
            for i=1:data.number_obs
                plot(Polyhedron(data.polytopes.obs{i}.P,data.polytopes.obs{i}.q),'color','black');
            end
            
            % Plot targets
            for i=1:data.number_tar
                plot(Polyhedron(data.polytopes.tar{i}.P,data.polytopes.tar{i}.q),'color','red');
            end
            
            % Initial positions
            for i = 1 : data.number_robots
                q = [data.agents{i}.x0(1);
                    data.agents{i}.x0(3);
                    -data.agents{i}.x0(1);
                    -data.agents{i}.x0(3)]+...
                    [data.polytopes.agent_body{i}.q(1)/2;
                    data.polytopes.agent_body{i}.q(2)/2;
                    +data.polytopes.agent_body{i}.q(3)/2;
                    +data.polytopes.agent_body{i}.q(4)/2];
% %                 
%                                  plot(obj.agents{i}.x0(1),obj.agents{i}.x0(3),'o')
%                                   plot(Polyhedron(obj.polytopes.agent_body{i}.P,q),'color',obj.agents{i}.color);
            end
             
            
            
        end
        
        %%%%%%%%
        function PlotSnapshots(obj,data)
            
            
            
            
            figure(); hold on; grid;
             for k = 1 : data.N_opt
                 obj.PlotScenario(k,data);
                 for i = 1 : data.number_robots
                     plot(data.traj(1,1:k,i),data.traj(2,1:k,i),'color',obj.colors(i,:),'MarkerFaceColor', obj.colors(i,:));
                     plot(data.traj(1,k,i),data.traj(2,k,i),'o','color',obj.colors(i,:),'MarkerFaceColor', obj.colors(i,:));
                 end

             end
            
            
        end
        
        %%%%%%%%%
        function PlotCommunicationNetwork(obj,data)
            
            n_col = 3;
            n_row = 3;
            j = 0;
            figure;
            for k = 1 : data.N_opt
                names = [];
                aux = 0;
                names = ["Base"];
                for i=2:data.number_robots
                    names = [names,"Relay"+(i-1)];
                end
                
                Adj = zeros(data.number_robots);
                for i = 1 : data.number_robots-1
                    for j = i + 1 : data.number_robots

                            Adj(i,j) = data.B_con(data.index.B_con(k,i,j));
                            Adj(j,i) = data.B_con(data.index.B_con(k,i,j));
                            %Adj(j,i) = obj.mip.opt_vars.B_con(obj.mip.index.B_con(k,i,j));
                    end
                end
                
                Adj = round(Adj,1);
                
                
                names = [names,"Leader"];
                
                %dig = graph(Adj,names);
                grp = graph(Adj);
                subplot(n_row,n_col,k)
                plot(grp);
                title("k="+k);
                
                
            end
            
            
        end
        
        %%%%%%%%%
        function PlotOrientations(obj,data)
            max_number_subplot_columns = 3;
            column_coord = 0;
            row_coord = 1;

            for i = 1 : data.number_robots
                 column_coord = column_coord + 1;
                if column_coord >  max_number_subplot_columns
                    row_coord = row_coord + 1;
                
                 column_coord= 1;
                end
                plot_coord(:,i) = [row_coord;column_coord];
            end
            
            figure(); 
            for i = 1 : data.number_robots
                for k = 1 : data.N_opt
                    orientation(k,i) = (pi/12)*(find(round(data.B_orient(data.index.B_orient(k,i,:),1))==1)-1);
                    subplot(size(plot_coord,1),min(data.number_robots,max_number_subplot_columns),i);
                    plot(k-1,orientation(k,i)*180/pi,'o'); hold on; grid on; 
                    xlim([0 data.N_opt]); ylim([0 360]);
                    xticks([0:1:data.N_opt]);
                    yticks([0:60:360]);
                    xlabel('Time (s)'); ylabel('Orientation (degrees)');
                    title("Robot "+i)
                    
                end
                hold off;
                
            end
            orientation
        end
        
        %%%%%%%%%
        
        function PlotLinearVelocities(obj,data)
            
            max_number_subplot_columns = 3;
            column_coord = 0;
            row_coord = 1;
            
            for i = 1 : data.number_robots
                column_coord = column_coord + 1;
                if column_coord >  max_number_subplot_columns
                    row_coord = row_coord + 1;
                    
                    column_coord= 1;
                end
                plot_coord(:,i) = [row_coord;column_coord];
            end
            
            figure(); 
            
            for i = 1 : data.number_robots
%                    for k = 1 : data.N_opt
%                        lin_vel(k,i) = norm(data.vel(:,k,i));
%                    end
                       
                
                    subplot(size(plot_coord,1),min(data.number_robots,max_number_subplot_columns),i);
                    plot([0:data.N_opt-1],value(data.lin_vel(data.index.lin_vel(1:data.N_opt,i))),'o'); grid;
                   % plot([0:data.N_opt-1],lin_vel(:,i)); grid;
                    xlim([0 data.N_opt]); ylim([0 1]);
                    xticks([0:1:data.N_opt]);
                    %yticks([0:60:360]);
                    xlabel('Time (s)'); ylabel('Linear velocity (m/s)');
                    title("Robot "+i)
            end
            
            
            
        end
        
        function PlotAccelerations(obj,data)
            
            max_number_subplot_columns = 3;
            column_coord = 0;
            row_coord = 1;
            
            for i = 1 : data.number_robots
                column_coord = column_coord + 1;
                if column_coord >  max_number_subplot_columns
                    row_coord = row_coord + 1;
                    
                    column_coord= 1;
                end
                plot_coord(:,i) = [row_coord;column_coord];
            end
            
            figure(); 
            
            for i = 1 : data.number_robots
%                    for k = 1 : data.N_opt
%                        lin_vel(k,i) = norm(data.vel(:,k,i));
%                    end
                       
                
                    subplot(size(plot_coord,1),min(data.number_robots,max_number_subplot_columns),i);
                    plot([0:data.N_opt-1],value(data.acc(data.index.lin_vel(1:data.N_opt,i))),'o'); grid;
                   % plot([0:data.N_opt-1],lin_vel(:,i)); grid;
                    xlim([0 data.N_opt]); ylim([-1 1]);
                    xticks([0:1:data.N_opt]);
                    %yticks([0:60:360]);
                    xlabel('Time (s)'); ylabel('Acceleration (m/s^2)');
                    title("Robot "+i)
            end
            
            
            
        end
        
        function debug(obj)
            figure(); hold on; grid;
            for k = 1 : obj.N_opt
                obj.scenario.Plot(k);
                for i = 2 : 2
                    plot(obj.traj(1,1:k,i),obj.traj(2,1:k,i),'LineWidth',2,'color',obj.colors(i,:));
                    % Plot position
                    switch i 
                        case 1 % base
                        plot(obj.traj(1,k,i),obj.traj(2,k,i),'s','color',obj.colors(i,:),'MarkerSize',15,'MarkerFaceColor',obj.colors(i,:));    
                        case obj.mip.number_robots
                        plot(obj.traj(1,k,i),obj.traj(2,k,i),'^','color',obj.colors(i,:),'MarkerSize',10,'MarkerFaceColor',obj.colors(i,:));        
                        otherwise
                       plot(obj.traj(1,k,i),obj.traj(2,k,i),'o','color',obj.colors(i,:),'MarkerSize',10,'MarkerFaceColor',obj.colors(i,:));
                    end
                    % Plot cone
                    if i < obj.mip.number_robots
                   angle_index = find(round(value(obj.mip.opt_vars.B_orient(obj.mip.index.B_orient(k,i,:))),0)==1);       
                   cone = Polyhedron(obj.scenario.polytopes.fov_cones{angle_index}.P,obj.scenario.polytopes.fov_cones{angle_index}.q);
                   cone_v = cone.computeVRep;
                   cone_v_trans = Polyhedron(cone_v.V+[ones(4,1)*obj.traj(1,k,i),ones(4,1)*obj.traj(2,k,i)]);
                   cone_v_trans.plot('color','yellow','alpha',0.2);
                   end
                    
                end
            end
            
          
        end
        
        
        function data = BuildFileForStorage(obj)
            data.traj = obj.traj;
            data.vel = obj.vel;

            data.number_robots = obj.mip.number_robots;
            data.polytopes = obj.scenario.polytopes;
            data.number_obs = obj.scenario.number_obs;
            data.number_tar = obj.scenario.number_tar;
            data.agents = obj.agents;
            data.B_con = value(obj.mip.opt_vars.B_con);
            data.index = obj.mip.index;
            data.N_opt = obj.N_opt;
        end
        
    end % end methods
end % end class

