function scenario_struct = GenRngScenObsSide(params)

number_obs_sides = params.number_obs_sides;
n_obstacles = params.number_obstacles;
scenario_struct.number_agents = params.number_agents;

%% Environment
op_region_width = 3;
op_region_length = 3;
op_region_center = [0,0];

%number_obs_sides = 5;
number_obs_vertices = number_obs_sides;

% Operation region ( static )
scenario_struct.polytopes.op_region.P = [eye(2);-eye(2)];
scenario_struct.polytopes.op_region.q = [op_region_center(1) + op_region_length/2;
    op_region_center(2) + op_region_width/2;
    op_region_center(1) + op_region_length/2;
    op_region_center(2) + op_region_width/2];


%% Obstacles ( random )

obs_max_length = 1;
obs_max_width = 1;

obs_region_length = 3;
obs_region_width = 3;


i = 1;

ellipse_dimensions = cell(n_obstacles,1);
ellipse_orientation = cell(n_obstacles,1);
ellipse = cell(n_obstacles,1);
ellipse_center =cell(n_obstacles,1);

%figure; hold; xlim([-1.5 1.5]); ylim([-1.5 1.5]);
while i <= n_obstacles

    
    % Determines random dimensions of a box containing the ellipse
    ellipse_dimensions{i} = [rand(1,1)*obs_max_length,rand(1,1)*obs_max_width];
    
    % Randomly determines ellipse's orientation
    ellipse_orientation{i} = 2*pi*rand(1,1);
    
    
    % Builds random rotated ellipse
    angles = [0:0.01:2*pi];
    
    unrotated_ellipse = [ellipse_dimensions{i}(1)/2.*cos(angles); ellipse_dimensions{i}(2)/2.*sin(angles)];
    
    R = [cos(ellipse_orientation{i}) sin(ellipse_orientation{i});-sin(ellipse_orientation{i}) cos(ellipse_orientation{i})];
    for j = 1 : length(unrotated_ellipse); ellipse{i}(:,j) = R*unrotated_ellipse(:,j); end
    
    % Randomly translates ellipse
    x_sup_bound = obs_region_length/2-max(ellipse{i}(1,:));
    x_inf_bound = -(obs_region_length/2) - min(ellipse{i}(1,:));
    y_sup_bound = obs_region_width/2-max(ellipse{i}(2,:));
    y_inf_bound = -(obs_region_width/2) - min(ellipse{i}(2,:));
    ellipse_center{i} = [x_inf_bound + (-x_inf_bound + x_sup_bound)*rand(1,1);
        y_inf_bound + (-y_inf_bound + y_sup_bound)*rand(1,1)];
    
    ellipse{i}(1,:) = ellipse{i}(1,:) + ellipse_center{i}(1);
    ellipse{i}(2,:) = ellipse{i}(2,:) + ellipse_center{i}(2);
    
    % Randomly samples the ellipse to determine obstacle vertices
    ellipse_sample_index = randi([1 length(angles)],number_obs_vertices,1);
    vertices = ellipse{i}(:,ellipse_sample_index)';
    
    % Generates polygon
    G = Polyhedron(vertices);
   % G = G.minHRep;
    scenario_struct.polytopes.obs{i}.P = G.A;
    scenario_struct.polytopes.obs{i}.q = G.b;
    
    
    % Checks if there is intersection between new obstacles
    for j = i - 1 : -1 : 1
        poly1 = Polyhedron(scenario_struct.polytopes.obs{i}.P,scenario_struct.polytopes.obs{i}.q);
        poly2 = Polyhedron(scenario_struct.polytopes.obs{j}.P,scenario_struct.polytopes.obs{j}.q);
        poly3 = poly1.intersect(poly2);
        
        if ~poly3.isEmptySet
            i = i - 1;
        else
           
            % Checks if it has the correct number of sides, sometimes it has less
            % sides and this breaks the code
            if length(G.A) ~= number_obs_sides
                i = i - 1;
            end
            
        end
        
    end
    
    i = i + 1;
    
end

scenario_struct.number_obs = length(ellipse_center);
%% targets
scenario_struct.number_tar = 0;


%% Agents
% Number of agents

% All agents are equal in this case
A = [0 1 0 0; 0 0 0 0;0 0 0 1; 0 0 0 0];
scenario_struct.nx = size(A,1);
B = [0 0; 1 0;0 0;0 1];
scenario_struct.nu = size(B,2);
C = [1 0 0 0;0 0 1 0];
scenario_struct.ny = size(C,1);
D = zeros(scenario_struct.ny,scenario_struct.nu);
Ts = 1; % seconds;

i = 1;
while i <= scenario_struct.number_agents
    
    % Randomly selects initial position
    rng_pos = [(-obs_region_length)/2 + (obs_region_length)*rand(1,1);
        (-obs_region_width)/2 + (obs_region_width)*rand(1,1)];
    
    scenario_struct.agents{i}.x0 = [rng_pos(1);0;rng_pos(2);0];
    
    % Dynamics
    scenario_struct.agents{i}.dyn = c2d(ss(A,B,C,D),Ts);
    
    % Body polytope
    scenario_struct.polytopes.agent_body{i}.P = [eye(2);-eye(2)];
    scenario_struct.polytopes.agent_body{i}.q = [0;0;0;0];
    
    % Verifies if inside obstalce
    for j = 1 : scenario_struct.number_obs
        
        if all(scenario_struct.polytopes.obs{j}.P*rng_pos <= scenario_struct.polytopes.obs{j}.q)
            i = i - 1;
        end
        
    end
    
    i = i + 1;
end

% Connectivity region
connRadius = 1;
scenario_struct.polytopes.conn_region.P = [eye(2);-eye(2);-1 1;1 1;1 -1;-1 -1];
scenario_struct.polytopes.conn_region.q = [ones(4,1)*connRadius; 1.5*ones(4,1)*connRadius];
end

