% This script just loads and plots results stored in the 'results' folder


clearvars; clc; close all;
n_trials = 300;


for i = 1 : n_trials
    
    load("./results/4sides/data_4sides_"+i);
    data_4sides{i} = data_save;
    
opt_time_new_4sides(i) = data_4sides{i}.new.opt_time;
opt_time_old_4sides(i) = data_4sides{i}.old.opt_time;
    
end


for i = 1 : n_trials
    
  %  if i < 150
  %  load("./results/6sides/old/data_6sides_"+i);
   % else
        load("./results/6sides/data_6sides_"+i);
   % end
    data_6sides{i} = data_save;
    
opt_time_new_6sides(i) = data_6sides{i}.new.opt_time;
opt_time_old_6sides(i) = data_6sides{i}.old.opt_time;
    
end

for i = 1 : n_trials
    
    load("./results/8sides/data_8sides_"+i);
    data_8sides{i} = data_save;
    
opt_time_new_8sides(i) = data_8sides{i}.new.opt_time;
opt_time_old_8sides(i) = data_8sides{i}.old.opt_time;
    
end

for i = 1 : n_trials
    
    load("./results/10sides/data_10sides_"+i);
    data_10sides{i} = data_save;
    
opt_time_new_10sides(i) = data_10sides{i}.new.opt_time;
opt_time_old_10sides(i) = data_10sides{i}.old.opt_time;
    
end

% opt_time_new_6sides(205) = [];
% opt_time_old_6sides(205) = [];

%%
close all;

fig = figure; hold; grid;
scat1 = scatter(opt_time_new_4sides,opt_time_old_4sides,'o','MarkerFaceColor','r','MarkerFaceAlpha',0.2,'MarkerEdgeColor','r','MarkerEdgeAlpha',0.0);
scat2 = scatter(opt_time_new_6sides,opt_time_old_6sides,'o','MarkerFaceColor','g','MarkerFaceAlpha',0.2,'MarkerEdgeColor','r','MarkerEdgeAlpha',0.0);
scat3 = scatter(opt_time_new_8sides,opt_time_old_8sides,'o','MarkerFaceColor','b','MarkerFaceAlpha',0.2,'MarkerEdgeColor','r','MarkerEdgeAlpha',0.0);
scat4 = scatter(opt_time_new_10sides,opt_time_old_10sides,'o','MarkerFaceColor','m','MarkerFaceAlpha',0.2,'MarkerEdgeColor','r','MarkerEdgeAlpha',0.0);

plot([1:0.1:2.2],[1:0.1:2.2],'k--');
xlim([1 2.2]);ylim([1 2.2]);
xlabel('Opt. Time using novel approach (s)');
ylabel('Opt. Time using earlier approach (s)');
legend('4 Sides','6 Sides','8 Sides','10 Sides');

exportgraphics(fig,'opt_times.pdf','ContentType','vector')


figure; hold; grid;
markersize=2;
xticks([4:2:10])


plot([4,6,8,10],[mean(opt_time_new_4sides(1:n_trials)),mean(opt_time_new_6sides(1:n_trials)),mean(opt_time_new_8sides(1:n_trials)),mean(opt_time_new_10sides(1:n_trials))],'k')
plot([4,6,8,10],[mean(opt_time_old_4sides(1:n_trials)),mean(opt_time_old_6sides(1:n_trials)),mean(opt_time_old_8sides(1:n_trials)),mean(opt_time_old_10sides(1:n_trials))],'r')


xlabel('Number of Obstacle Sides');
ylabel('Mean Optimization Time (s)');
% 4 sides
ci_4sides_new = bootci(n_trials,@(x)mean(x),opt_time_new_4sides(1:n_trials))
lowerBarLengths = mean(opt_time_new_4sides(1:n_trials))-ci_4sides_new(1);
upperBarLengths = ci_4sides_new(2)-mean(opt_time_new_4sides(1:n_trials));
errorbar(4,mean(opt_time_new_4sides(1:n_trials)),lowerBarLengths,upperBarLengths,'ko','MarkerFaceColor','k','MarkerSize',markersize)

ci_4sides_old = bootci(n_trials,@(x)mean(x),opt_time_old_4sides(1:n_trials))
lowerBarLengths = mean(opt_time_old_4sides(1:n_trials))-ci_4sides_old(1);
upperBarLengths = ci_4sides_old(2)-mean(opt_time_old_4sides(1:n_trials));
errorbar(4,mean(opt_time_old_4sides(1:n_trials)),lowerBarLengths,upperBarLengths,'ro','MarkerFaceColor','r','MarkerSize',markersize)


% 6 sides
ci_6sides_new = bootci(n_trials,@(x)mean(x),opt_time_new_6sides(1:n_trials))
lowerBarLengths = mean(opt_time_new_6sides(1:n_trials))-ci_6sides_new(1);
upperBarLengths = ci_6sides_new(2)-mean(opt_time_new_6sides(1:n_trials));
errorbar(6,mean(opt_time_new_6sides(1:n_trials)),lowerBarLengths,upperBarLengths,'ko','MarkerFaceColor','k','MarkerSize',markersize)

ci_6sides_old = bootci(n_trials,@(x)mean(x),opt_time_old_6sides(1:n_trials))
lowerBarLengths = mean(opt_time_old_6sides(1:n_trials))-ci_6sides_old(1);
upperBarLengths = ci_6sides_old(2)-mean(opt_time_old_6sides(1:n_trials));
errorbar(6,mean(opt_time_old_6sides(1:n_trials)),lowerBarLengths,upperBarLengths,'ro','MarkerFaceColor','r','MarkerSize',markersize)

% 8 sides
ci_8sides_new = bootci(n_trials,@(x)mean(x),opt_time_new_8sides(1:n_trials))
lowerBarLengths = mean(opt_time_new_8sides(1:n_trials))-ci_8sides_new(1);
upperBarLengths = ci_8sides_new(2)-mean(opt_time_new_8sides(1:n_trials));
errorbar(8,mean(opt_time_new_8sides(1:n_trials)),lowerBarLengths,upperBarLengths,'ko','MarkerFaceColor','k','MarkerSize',markersize)

ci_8sides_old = bootci(n_trials,@(x)mean(x),opt_time_old_8sides(1:n_trials))
lowerBarLengths = mean(opt_time_old_8sides(1:n_trials))-ci_8sides_old(1);
upperBarLengths = ci_8sides_old(2)-mean(opt_time_old_8sides(1:n_trials));
errorbar(8,mean(opt_time_old_8sides(1:n_trials)),lowerBarLengths,upperBarLengths,'ro','MarkerFaceColor','r','MarkerSize',markersize)

% 10 sides
ci_10sides_new = bootci(n_trials,@(x)mean(x),opt_time_new_10sides(1:n_trials))
lowerBarLengths = mean(opt_time_new_10sides(1:n_trials))-ci_10sides_new(1);
upperBarLengths = ci_10sides_new(2)-mean(opt_time_new_10sides(1:n_trials));
errorbar(10,mean(opt_time_new_10sides(1:n_trials)),lowerBarLengths,upperBarLengths,'ko','MarkerFaceColor','k','MarkerSize',markersize)

ci_10sides_old = bootci(n_trials,@(x)mean(x),opt_time_old_10sides(1:n_trials))
lowerBarLengths = mean(opt_time_old_10sides(1:n_trials))-ci_10sides_old(1);
upperBarLengths = ci_10sides_old(2)-mean(opt_time_old_10sides(1:n_trials));
errorbar(10,mean(opt_time_old_10sides(1:n_trials)),lowerBarLengths,upperBarLengths,'ro','MarkerFaceColor','r','MarkerSize',markersize)

