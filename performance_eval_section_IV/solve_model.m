function results = solve_model(model,isNewConst)

if isNewConst
    los_constraints= [model.constraints.los
    model.constraints.los_bin
   ];
    
else
    los_constraints = [model.constraints.old_los
     model.constraints.bin_old_los
    model.constraints.los_corner_cut];
    
end



options = sdpsettings('solver','gurobi');


constraints = [
     model.constraints.initial_condition
     model.constraints.obstacles
     model.constraints.obstacles_binary 
     los_constraints
    ];


disp("Starting optimization");
timer = tic;
diagnostics = optimize(constraints,model.cost,options);
opt_time = toc(timer);
display("Optimization time: "+opt_time);

results.opt_time = opt_time;
results.MIP = model;
results.diag = diagnostics;



end

