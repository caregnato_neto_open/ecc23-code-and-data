# European Control Conference 23 code and data

## Correction note:

There is a typo in constraint (5l) as there is no connectivity binary implication. Correction:

$\text{deg}_i(k+1) + \text{deg}_j(k+1) \geq   (n_a-1)$


## Introduction

This repository contains all code, data, and supplementary material associated with the paper "A Novel Line of Sight Constraint for Mixed-Integer Programming Models with Applications to Multi-Agent Motion Planning", which is to be presented in the 21st European Control Conference.

## Dependencies

- MATLAB (functional on version R2020b)
- YALMIP R20210331 (https://yalmip.github.io/)
- Multi-Parametric Toolbox R2011a (https://www.mpt3.org/)

## Instructions

The code for motion planning simulations can be found in the folder 'motion_planning_section_IIIB'. The code and data of the performance evaluation are located in the folder 'performance_eval_section_IV'. The suplementary discussion of the number of constraints added can be found in the 'constraint_evaluation' PDF.


