classdef AnalysisClass < handle

  
    properties
        mip = [];
        env = [];
        N_opt = [];
        traj = [];
        vel = [];
        acc = [];
        color = [];
        agents = [];
        number_agents = []
    end
    
    methods
        function Construct(obj,mip,env,agents)
            
            obj.mip = mip;
            obj.env = env;
            obj.N_opt = find(value(mip.opt_vars.B_hor) == 1);
            obj.agents = agents;
            obj.number_agents = length(agents);
            obj.BuildColors();

            
            % pos
            for i = 1 : obj.number_agents
                for k = 1 : obj.N_opt
                    obj.traj(:,k,i) = [value(mip.opt_vars.X(mip.index.x(1,k,i)));value(mip.opt_vars.X(mip.index.x(3,k,i)))];
                end
            end
            
            
            % vel
            for i = 1 : obj.number_agents
                for k = 1 : obj.N_opt
                    obj.vel(:,k,i) = [value(mip.opt_vars.X(mip.index.x(2,k,i)));value(mip.opt_vars.X(mip.index.x(4,k,i)))];
                end
            end
            
                  % acc
            for i = 1 : obj.number_agents
                for k = 1 : obj.N_opt-1
                    obj.acc(:,k,i) = [value(mip.opt_vars.U(mip.index.u(1,k,i)));value(mip.opt_vars.U(mip.index.u(2,k,i)))];
                end
            end
            
        end
        
        function PlotTrajectories(obj)
            
            obj.env.Plot();
            for i = 1 : obj.number_agents
                plot(obj.traj(1,1:obj.N_opt,i),obj.traj(2,1:obj.N_opt,i),obj.color{i});
                for k = 1 : obj.N_opt-1
                    if k == 1 % initial pos
                        plot(obj.traj(1,k,i),obj.traj(2,k,i),"o",'MarkerFaceColor',obj.color{i},'MarkerSize',6,'MarkerEdgeColor',obj.color{i});
                    else % in between positions
                        plot(obj.traj(1,k,i),obj.traj(2,k,i),'square','MarkerFaceColor',obj.color{i},'MarkerSize',6,'MarkerEdgeColor',obj.color{i});
                    end
                end
                plot(obj.traj(1,obj.N_opt,i),obj.traj(2,obj.N_opt,i),'^','MarkerFaceColor',obj.color{i},'MarkerSize',6,'MarkerEdgeColor',obj.color{i}); 
            end
            xlabel('r_x'); ylabel('r_y');
            
        end
        
        function BuildColors(obj)
            color_list = {'r','g','b','m'};
            for i = 1 : obj.number_agents; obj.color{i} = color_list{i}; end
        end
        
        function PlotVelocities(obj,data)
            
            figure();  xlabel('v_x'); ylabel('v_y');
            for i=1:24
                plot(Polyhedron(data.polytopes.orient{i}.P,data.polytopes.orient{i}.q)); hold on; 
            end

            for j = 1 : obj.number_agents
                for k = 1 : data.N_opt 
                    orientation(k,j) = (pi/12)*(find(round(data.B_orient(data.index.B_orient(k,j,:),1))==1)-1);
                    plot(value(data.lin_vel(data.index.lin_vel(k,j)))*cos(orientation(k,j)),value(data.lin_vel(data.index.lin_vel(k,j)))*sin(orientation(k,j)),'o');
                end
           % plot(data.vel(1,1:data.N_opt,j),data.vel(2,1:data.N_opt,j),'o');
           
            end
            
        end
        
      
        
    end % end methods
end % end class

