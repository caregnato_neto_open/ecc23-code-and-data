function results = run_opt(env,agents)

MIP = MIPClass(agents,env.polytopes);
MIP.BuildOptVarIndexes();

% Contraints
MIP.BuildLinearDynamicConstraints(agents);
MIP.BuildPositionAndInputConstraints(env.polytopes);
MIP.BuildTargetConstraints(env.polytopes);
MIP.BuildObstacleAvoidanceConstraints(env.polytopes);
MIP.BuildCollisionAvoidanceConstraints(agents);
MIP.BuildConnectivityConstraints();
MIP.BuildIntermediaryPointsLOSConstraint(env.polytopes)
MIP.BuildAbsolutValueInput();

MIP.BuildConnectivityRegionConstraint(agents);

%% Solve

constraints = [
    MIP.constraints.dyn
    MIP.constraints.initial_condition
    MIP.constraints.position
    MIP.constraints.acceleration
    MIP.constraints.velocity
    MIP.constraints.targets
    MIP.constraints.target_hor_eq
    MIP.constraints.tar_limit
    MIP.constraints.no_reward_after_horizon
    MIP.constraints.horizon
    MIP.constraints.obstacles
    MIP.constraints.obstacles_binary
    MIP.constraints.obstacles_corner_cutting
    MIP.constraints.collision_avoidance
    MIP.constraints.collision_binary
    MIP.constraints.collision_corner_cutting
    MIP.constraints.conn
    MIP.constraints.los
    MIP.constraints.los_bin
    MIP.constraints.conn_region
    MIP.constraints.abs_input
    ];
% Cost
MIP.BuildCost();

% Star optimization
options = sdpsettings('solver','gurobi');
optimize(constraints,MIP.cost,options);
results.MIP = MIP;

end

