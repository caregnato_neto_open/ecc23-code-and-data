clc; clearvars; close all; yalmip('clear')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MIP motion planning under Line-of-Sight (LOS) constraints as discussed in
% section III-B of
%
% "A Novel Line of Sight Constraint for Mixed-Integer Programming
%  Models with Applications to Multi-Agent Motion Planning"
%
% Angelo Caregnato-Neto, Marcos Omena de Albuquerque Maximo, and Rubens
% Junqueira Magalhães Afonso (Instituto Tecnológico de Aeronáutica)
%
% Presented in the 21st European Control Conference, Bucharest, Romania.
%
% This code is far from optimized as its purpose is merely reproducibility
%
% It is compatible with MATLAB 2020b and requires the YALMIP toolbox and
% some MIP solver. Here it is hardcoded for gurobi.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Build environment
env = environmentClass(get_environment());
agents = get_agents(); 
    
% Solve motion planning problem
disp("Starting optimization");
results = run_opt(env,agents);
       
%% plots
close all;
disp("Plotting trajectories");

analysis = AnalysisClass;
analysis.Construct(results.MIP,env,agents);
analysis.PlotTrajectories()





