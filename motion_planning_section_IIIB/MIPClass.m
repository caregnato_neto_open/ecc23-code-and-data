classdef MIPClass < handle
    
    properties
        nx = [];
        nu = [];
        number_tar = [];
        number_obs = [];
        number_agents = [];
        number_obs_sides = [];
        number_body_sides = [];
        number_angles = [];
        N = [];
        BigM = [];
        opt_vars = [];
        constraints = [];
        index = [];
        cost = []
        control_weight = [];
        beta = [];
    end
    
    methods
        
        function MIP = MIPClass(agents,poly)
            % Auxiliary vars
            MIP.nx = size(agents{1}.dyn.a,1);
            MIP.nu = size(agents{1}.dyn.b,2);
            MIP.number_agents = length(agents);
            MIP.number_obs = length(poly.obs);
            MIP.number_tar = length(poly.tar);
            MIP.number_obs_sides = size(poly.obs{1}.P,1);
            MIP.number_body_sides = size(agents{1}.polytopes.agent_body.P,1);
            
            % Optimization parameters
            MIP.N = 8;
            MIP.BigM = 100;
            MIP.control_weight = 10;
            n_los_points = 4;
            MIP.beta = [1/(n_los_points+1):1/(n_los_points+1):1-(1/(n_los_points+1))];
            
            % Real-valued optimization variables
            MIP.opt_vars.X = sdpvar((MIP.N+1)*MIP.number_agents*MIP.nx,1);
            MIP.opt_vars.U = sdpvar(MIP.N*MIP.nu*MIP.number_agents,1);
            MIP.opt_vars.Abs_U = sdpvar(MIP.N*MIP.nu*MIP.number_agents,1);
            
            % Binary variables
            MIP.opt_vars.B_hor = binvar(MIP.N+1,1);
            MIP.opt_vars.B_tar = binvar((MIP.N+1)*MIP.number_tar*MIP.number_agents,1);
            MIP.opt_vars.B_obs = binvar((MIP.N+1)*MIP.number_agents*MIP.number_obs*MIP.number_obs_sides,1);
            MIP.opt_vars.B_col = binvar((MIP.N+1)*sum(1:MIP.number_agents-1)*MIP.number_body_sides,1);
            MIP.opt_vars.B_con = binvar((MIP.N+1)*sum(1:MIP.number_agents-1),1);
            MIP.opt_vars.B_los = binvar(MIP.number_obs*length(MIP.beta)*(MIP.N+1)*sum(1:MIP.number_agents-1),1);

        end
        
        function BuildOptVarIndexes(MIP)
            
            counter = 0;
            counter_u = 0;
            counter_obs = 0;
            counter_col = 0;
            counter_tar = 0;
            counter_con = 0;
            counter_los = 0;
            counter_los_old = 0;
            
            
            for i = 1 : MIP.number_agents
                for k = 1 : MIP.N+1
                    
                    counter = counter + 1;
                    % linear velocity (nu)
                    MIP.index.lin_vel(k,i) = counter;
                    
                    
                    % old los const
                    if i <= MIP.number_agents-1
                        for j = i + 1 : MIP.number_agents
                            for n = 1 : MIP.number_obs
                                for z = 1 : length(MIP.beta)-1
                                    for g = 1 : MIP.number_obs_sides
                                        counter_los_old = counter_los_old + 1;
                                        MIP.index.B_old_los(g,z,n,k,i,j) = counter_los_old;
                                    end
                                end
                            end
                            
                        end
                    end
                    
                    % state
                    MIP.index.x(:,k,i) = (counter-1)*MIP.nx+1:counter*MIP.nx;
                    
                    % obs bin
                    for j = 1 : MIP.number_obs
                        for n = 1 : MIP.number_obs_sides
                            counter_obs =  counter_obs + 1;
                            MIP.index.B_obs(n,k,i,j) = counter_obs; %...
                            %(counter-1)*MIP.number_obs_sides+1:counter*MIP.number_obs_sides;
                        end
                    end
                    
                    % col bin
                    if i <= MIP.number_agents-1
                        for j = i + 1 : MIP.number_agents
                            for m = 1 : MIP.number_body_sides
                                counter_col = counter_col + 1;
                                MIP.index.B_col(m,k,i,j) = counter_col;
                                %= (counter-1)*MIP.number_body_sides+1:counter*MIP.number_body_sides;
                            end
                        end
                    end
                    
                    % con bin
                    if i <= MIP.number_agents-1
                        for j = i + 1 : MIP.number_agents
                            counter_con = counter_con + 1;
                            MIP.index.B_con(k,i,j) = counter_con;
                        end
                    end

                 % b_los
                    if i <= MIP.number_agents-1
                        for j = i + 1 : MIP.number_agents
                            for z = 1 : length(MIP.beta)
                                for w = 1 : MIP.number_obs
                                counter_los = counter_los + 1;
                                MIP.index.B_los(j,i,z,k,w) =  counter_los;
                                end
                            end
                        end
                    end
                    
                end
                
                % input (outside time loop to N+1)
                for z = 1 : MIP.N
                    for ww = 1 : MIP.nu
                        counter_u = counter_u + 1;
                        MIP.index.u(ww,z,i) = counter_u; % input
                    end
                end
                
            end
            
            
            
            
            % target bin
            for k = 1 : MIP.N+1
                for i = 1 : MIP.number_agents
                    for j = 1 : MIP.number_tar
                        counter_tar = counter_tar + 1;
                        MIP.index.B_tar(k,j,i) = counter_tar;
                    end
                end
            end
            
            
        end
        
        function BuildAbsolutValueInput(MIP)

           
           MIP.constraints.abs_input = [-MIP.opt_vars.U <= MIP.opt_vars.Abs_U,
                                         MIP.opt_vars.U <= MIP.opt_vars.Abs_U];
            
            
        end
        
        function BuildLinearDynamicConstraints(MIP,agents)
            A = agents{1}.dyn.a;
            B = agents{1}.dyn.b;
            
            MIP.constraints.dyn = [];
            MIP.constraints.initial_condition = [];
            for i = 1 : MIP.number_agents
                
                % Initial conditions
                MIP.constraints.initial_condition = [MIP.constraints.initial_condition,
                    MIP.opt_vars.X(MIP.index.x(:,1,i)) == agents{i}.x0,
                    ];
                
                % Dynamics
                for k = 1 : MIP.N
                    
                    MIP.constraints.dyn = [MIP.constraints.dyn,
                        MIP.opt_vars.X(MIP.index.x(:,k+1,i)) <= A*MIP.opt_vars.X(MIP.index.x(:,k,i))+B*MIP.opt_vars.U(MIP.index.u(:,k,i)) + ones(MIP.nx,1)*sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM];
                    
                    MIP.constraints.dyn = [MIP.constraints.dyn,
                        -MIP.opt_vars.X(MIP.index.x(:,k+1,i)) <= -A*MIP.opt_vars.X(MIP.index.x(:,k,i))-B*MIP.opt_vars.U(MIP.index.u(:,k,i)) + ones(MIP.nx,1)*sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM];
                    
                end
            end
        end
        
        function BuildPositionAndInputConstraints(MIP,poly)
            
            rx_max = poly.op_region.q(1);
            ry_max = poly.op_region.q(2);
            rx_min = -poly.op_region.q(3);
            ry_min = -poly.op_region.q(4);
            
            MIP.constraints.acceleration = [];
            MIP.constraints.velocity = [];
            MIP.constraints.position = [];
            
            
            % Agent 1
            for k = 1 : MIP.N
                
                %
                for i = 1 : MIP.number_agents
                    
                    
                    
                    pos = [MIP.opt_vars.X(MIP.index.x(1,k+1,i)); MIP.opt_vars.X(MIP.index.x(3,k+1,i))];
                    vel = [MIP.opt_vars.X(MIP.index.x(2,k+1,i)); MIP.opt_vars.X(MIP.index.x(4,k+1,i))];
                    acc = MIP.opt_vars.U(MIP.index.u(:,k));
                    
                    MIP.constraints.acceleration = [MIP.constraints.acceleration,
                        acc <= 0.75
                        acc >= -0.75
                        ];
                    
                    
                    % Velocity
                    MIP.constraints.velocity = [MIP.constraints.velocity,
                        vel <= 1 + sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM,
                        vel >= -1 - sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM];
                    
                    
                    % State
                    MIP.constraints.position = [MIP.constraints.position,
                        pos <= [rx_max;ry_max] + ones(2,1)*sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM,
                        -pos <= -[rx_min;ry_min] + ones(2,1)*sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM];
                    
                    
                    
                end
            end
        end
        
        function BuildCollisionAvoidanceConstraints(MIP,agents)
            
            MIP.constraints.collision_avoidance = [];
            MIP.constraints.collision_binary = [];
            MIP.constraints.collision_corner_cutting = [];
            for k = 1 : MIP.N + 1
                for i = 1 : MIP.number_agents-1
                    
                    % pos of i-th robot at time step k
                    pos_i = [MIP.opt_vars.X(MIP.index.x(1,k,i));
                        MIP.opt_vars.X(MIP.index.x(3,k,i))];
                    
                    for j = i + 1 : MIP.number_agents
                        B_col = MIP.opt_vars.B_col(MIP.index.B_col(:,k,i,j));
                        
                        % pos of j-th robot at time step k
                        pos_j = [MIP.opt_vars.X(MIP.index.x(1,k,j));
                            MIP.opt_vars.X(MIP.index.x(3,k,j))];
                        
                        % Relative position
                        pos_diff = pos_i - pos_j;
                        
                        MIP.constraints.collision_avoidance = [MIP.constraints.collision_avoidance,
                            -agents{i}.polytopes.agent_body.P*pos_diff<=-agents{i}.polytopes.agent_body.q+(ones(4,1)-B_col)*MIP.BigM+ones(4,1)*sum(MIP.opt_vars.B_hor(1:k-1))*MIP.BigM];
                        
                        
                        if k <= MIP.N
                            
                            % Takes next position if i-th and j-th agents
                            pos_i_next = [MIP.opt_vars.X(MIP.index.x(1,k+1,i));
                                MIP.opt_vars.X(MIP.index.x(3,k+1,i))];
                            
                            pos_j_next = [MIP.opt_vars.X(MIP.index.x(1,k+1,j));
                                MIP.opt_vars.X(MIP.index.x(3,k+1,j))];
                            
                            % Next relative position
                            pos_diff_next = pos_i_next - pos_j_next;
                            
                            MIP.constraints.collision_corner_cutting = [MIP.constraints.collision_corner_cutting,
                                -agents{i}.polytopes.agent_body.P*pos_diff_next<=-agents{i}.polytopes.agent_body.q+(ones(4,1)-B_col)*MIP.BigM+ones(4,1)*sum(MIP.opt_vars.B_hor(1:k-1))*MIP.BigM];
                        end
                        
                        % obs binary constraint
                        MIP.constraints.collision_binary = [MIP.constraints.collision_binary,sum(B_col) >= 1];
                        
                        
                    end
                end
            end
            
        end
        
        function BuildTargetConstraints(MIP,poly)
            
            MIP.constraints.targets = [];
            MIP.constraints.target_hor_eq  = [];
            MIP.constraints.tar_limit = [];
            MIP.constraints.no_reward_after_horizon = [];
            
            % Target entry
            for k = 1 : MIP.N + 1
                for i = 1 : MIP.number_agents
                    pos = [MIP.opt_vars.X(MIP.index.x(1,k,i));MIP.opt_vars.X(MIP.index.x(3,k,i))];
                    
                    for t = 1 : MIP.number_tar
                        B_tar = MIP.opt_vars.B_tar(MIP.index.B_tar(k,t,i));
                        MIP.constraints.targets = [MIP.constraints.targets,
                            poly.tar{t}.P*pos <= poly.tar{t}.q + ones(length(poly.tar{t}.q),1)*(1-B_tar)*MIP.BigM];
                    end
                    
                end
                
            end
            
            % Target binary conditions
            for k = 1 : MIP.N+1
                % Horizon is equal to sum of last target entry binary
                MIP.constraints.target_hor_eq = [MIP.constraints.target_hor_eq,
                    sum(MIP.opt_vars.B_tar(MIP.index.B_tar(k,MIP.number_tar,:))) - MIP.opt_vars.B_hor(k) == 0];
            end
            
            % Limit target collection
            for t = 1 : MIP.number_tar
                aux = [];
                for k = 1 : MIP.N + 1
                    aux = [aux;sum(MIP.opt_vars.B_tar(MIP.index.B_tar(k,t,:)))];
                end
                MIP.constraints.tar_limit = [MIP.constraints.tar_limit, sum(aux)<=1];
            end
            
            % Avoid collection after horizon
            for t = 1 : MIP.number_tar
                for i = 1 : MIP.number_agents
                    
                    MIP.constraints.no_reward_after_horizon = [MIP.constraints.no_reward_after_horizon,
                        [1:MIP.N+1]*(MIP.opt_vars.B_tar(MIP.index.B_tar(:,t,i)) - MIP.opt_vars.B_hor) <= 0];
                end
            end
            
            MIP.constraints.horizon = [sum(MIP.opt_vars.B_hor) == 1];
            
        end
        
        function BuildObstacleAvoidanceConstraints(MIP,poly)
            
            MIP.constraints.obstacles = [];
            MIP.constraints.obstacles_binary = [];
            MIP.constraints.obstacles_corner_cutting = [];
            for i = 1 : MIP.number_agents
                for k = 1 : MIP.N+1
                    
                    % aux
                    pos = [MIP.opt_vars.X(MIP.index.x(1,k,i));
                        MIP.opt_vars.X(MIP.index.x(3,k,i))];
                    
                    if k <= MIP.N
                        pos_next = [MIP.opt_vars.X(MIP.index.x(1,k+1,i));
                            MIP.opt_vars.X(MIP.index.x(3,k+1,i))];
                    end
                    
                    for j = 1 : MIP.number_obs
                        B_obs = MIP.opt_vars.B_obs(MIP.index.B_obs(:,k,i,j));
                        
                        % Obs entry constraint
                        MIP.constraints.obstacles = [MIP.constraints.obstacles,
                            -poly.obs{j}.P*pos <= -poly.obs{j}.q+(ones(4,1)-B_obs)*MIP.BigM+ones(4,1)*sum(MIP.opt_vars.B_hor(1:k-1))*MIP.BigM];
                        
                        % Corner cutting
                        if k <= MIP.N
                            MIP.constraints.obstacles_corner_cutting = [MIP.constraints.obstacles_corner_cutting,
                                -poly.obs{j}.P*pos_next <= -poly.obs{j}.q+(ones(4,1)-B_obs)*MIP.BigM+ones(4,1)*sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM];
                        end
                        
                        % obs binary constraint
                        MIP.constraints.obstacles_binary = [MIP.constraints.obstacles_binary,
                            sum(B_obs) >= 1];
                    end
                end
            end
            
            
        end
        
        function BuildConnectivityConstraints(MIP)
            
            B_con = MIP.opt_vars.B_con;
            %MIP.index.B_con(k,i,j)
            for k = 1 : MIP.N + 1
                deg(1,k) = sum(B_con(MIP.index.B_con(k,1,2:end)));
                deg(MIP.number_agents,k) = sum(B_con(MIP.index.B_con(k,1:MIP.number_agents-1,MIP.number_agents)));
                for j= 2 : MIP.number_agents - 1
                    deg(j,k) = sum(B_con(MIP.index.B_con(k,j,j+1:end))) + sum(B_con(MIP.index.B_con(k,1:j-1,j)));
                end
            end
            
            % Alternative constraint on connectivity based on theorem 1. Less
            % restrictive than the condition from the corollary.
            MIP.constraints.conn = [];
            for k = 1 : MIP.N + 1
                for i = 1 : MIP.number_agents - 1
                    for j = i + 1 : MIP.number_agents
                        MIP.constraints.conn = [MIP.constraints.conn,...
                            deg(j,k) + deg(i,k) >= (MIP.number_agents-1)-B_con(MIP.index.B_con(k,i,j))*MIP.number_agents-sum(MIP.opt_vars.B_hor(1:k-1))*MIP.BigM];
                    end
                end
            end
        end
        
        function BuildIntermediaryPointsLOSConstraint(MIP,poly)
            
            B_obs = MIP.opt_vars.B_obs;
            B_los = MIP.opt_vars.B_los;
            B_con = MIP.opt_vars.B_con;
            
            
            MIP.constraints.los = [];
            MIP.constraints.los_bin = [];
            
            for k = 1 : MIP.N + 1
                
                for i = 1 : MIP.number_agents - 1
                    pos_i = [MIP.opt_vars.X(MIP.index.x(1,k,i));
                        MIP.opt_vars.X(MIP.index.x(3,k,i))];
                    
                    for j = i + 1 : MIP.number_agents
                        pos_j = [MIP.opt_vars.X(MIP.index.x(1,k,j));
                            MIP.opt_vars.X(MIP.index.x(3,k,j))];
                        
                        for n = 1 : MIP.number_obs
                            
                            for z = 1 : length(MIP.beta)
                                sample_ij = pos_i*(1-MIP.beta(z)) + pos_j*MIP.beta(z);
                                
                                MIP.constraints.los = [MIP.constraints.los,
                                    -poly.obs{n}.P*sample_ij <= -poly.obs{n}.q+(ones(4,1)-B_obs(MIP.index.B_obs(:,k,i,n)))*MIP.BigM+ones(4,1)*(1-B_los(MIP.index.B_los(j,i,z,k,n)))*MIP.BigM];
                                
                                MIP.constraints.los = [MIP.constraints.los,
                                    -poly.obs{n}.P*sample_ij <= -poly.obs{n}.q+(ones(4,1)-B_obs(MIP.index.B_obs(:,k,j,n)))*MIP.BigM+ones(4,1)*(1-B_los(MIP.index.B_los(j,i,z,k,n)))*MIP.BigM];
                                
                            end
                            MIP.constraints.los_bin = [MIP.constraints.los_bin,
                                B_con(MIP.index.B_con(k,i,j)) <= sum(B_los(MIP.index.B_los(j,i,:,k,n)))];
                            %    B_con(MIP.index.B_con(k,j,i)) <= sum(B_los(MIP.index.B_los(j,i,:,k)))
                        end
                    end
                end
            end
        end
        
        function BuildConnectivityRegionConstraint(MIP,agents)
            
            B_con = MIP.opt_vars.B_con;
            
            MIP.constraints.conn_region = [];
            for k = 1 : MIP.N + 1
                for i = 1 : MIP.number_agents - 1
                    pos_i = [MIP.opt_vars.X(MIP.index.x(1,k,i));
                        MIP.opt_vars.X(MIP.index.x(3,k,i))];
                    
                    
                    for j = i + 1 : MIP.number_agents
                        pos_j = [MIP.opt_vars.X(MIP.index.x(1,k,j));
                            MIP.opt_vars.X(MIP.index.x(3,k,j))];
                        
                        rel_pos = pos_i - pos_j;
                        MIP.constraints.conn_region = [MIP.constraints.conn_region,...
                            agents{1}.polytopes.conn_region.P*rel_pos <= agents{1}.polytopes.conn_region.q + ones(8,1)*(1-B_con(MIP.index.B_con(k,i,j)))*MIP.BigM];
                        %agents{1} because its always the same region
                        
                    end
                end
            end
        end
        
        function BuildCost(MIP)
           MIP.cost = [1:MIP.N+1]*MIP.opt_vars.B_hor + sum(MIP.opt_vars.Abs_U) - sum(MIP.opt_vars.B_tar);
        end
        
    end % end methods
end % end class

