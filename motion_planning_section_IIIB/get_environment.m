function env = get_environment()

% Operation region
env.polytopes.op_region.P = [eye(2);-eye(2)];
env.polytopes.op_region.q = [1.5;1.5;1.5;1.5];

% Obstacles
env.polytopes.obs{1}.P = [eye(2);-eye(2)];
env.polytopes.obs{1}.q = [-0.5; 0.6; 1.501;-0.4];
    
env.polytopes.obs{2}.P = [eye(2);-eye(2)];
env.polytopes.obs{2}.q = [0.1; 1.501; 0.1;-0.4];
    
env.polytopes.obs{3}.P = [eye(2);-eye(2)];
env.polytopes.obs{3}.q = [0.1; -0.4; 0.1; 1.501];
    
env.polytopes.obs{4}.P = [eye(2);-eye(2)];
env.polytopes.obs{4}.q = [1.0; -0.4; -0.099;0.6];
env.number_obs = length(env.polytopes.obs);


% Targets
env.polytopes.tar{1}.P = [eye(2);-eye(2)]; 
env.polytopes.tar{1}.q = [-0.4;-0.4;0.6;0.6];

env.polytopes.tar{2}.P = [eye(2);-eye(2)];
env.polytopes.tar{2}.q = [1.1;0.6;-0.9;-0.4];

env.polytopes.tar{3}.P = [eye(2);-eye(2)];
env.polytopes.tar{3}.q = [0.6;-0.9;-0.4;1.1];
env.number_tar = length(env.polytopes.tar);
    


