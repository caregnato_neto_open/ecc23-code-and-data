classdef environmentClass < handle
    
    
    properties
        polytopes = [];
        number_obs = [];
        number_tar = []; 
    end
    
    
    methods
                
        function obj = environmentClass(env)
            obj.polytopes = env.polytopes;
            obj.number_obs = env.number_obs;
            obj.number_tar = env.number_tar;
            
        end

        function fig = Plot(obj)
            % plot for validation
            
            % Plot operational region
            fig = figure(1); hold on;
            field = Polyhedron(obj.polytopes.op_region.P,obj.polytopes.op_region.q);
            field.plot('color','white','alpha',0.2)
            
            % Plot obstacles
            for i=1:obj.number_obs
                plot(Polyhedron(obj.polytopes.obs{i}.P,obj.polytopes.obs{i}.q),'color','black');
            end
            
            % Plot targets
            for i=1:obj.number_tar
                plot(Polyhedron(obj.polytopes.tar{i}.P,obj.polytopes.tar{i}.q),'color','white');
            end
        end
        
        
    end
    
    
end