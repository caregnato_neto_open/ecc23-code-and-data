function agents = get_agents()

% Initial positions
agents{1}.x0 = [-1.25;0;1;0];
agents{2}.x0 = [-0.75;0;1;0];
agents{3}.x0 = [-1.3;0;1.2;0];
agents{4}.x0 = [-0.65;0;1.3;0];
number_agents = length(agents);

% Sampling period
Ts = 1;

% Dynamics (double integrators) and body (squares)
for i = 1 : number_agents
    
    % Continuous state-space
    A = [0 1 0 0; 0 0 0 0; 0 0 0 1; 0 0 0 0];
    nx = length(A);
    
    B = [0 0;1 0;0 0;0 1];
    nu = size(B,2);
    
    C = eye(nx);
    ny = size(C,1);
    
    D = zeros(ny,nu);
    
    % Discretize
    [Ad,Bd,Cd,Dd] = c2dm(A,B,C,D,Ts);
    
    agents{i}.nx = nx;
    agents{i}.nu = nu;
    agents{i}.ny = ny;
    agents{i}.dyn.a = Ad;
    agents{i}.dyn.b = Bd;
    agents{i}.dyn.c = Cd;
    agents{i}.dyn.d = Dd;
    
    % Body
    agents{i}.polytopes.agent_body.P = [eye(2);-eye(2)];
    agents{i}.polytopes.agent_body.q = [0.1; 0.1; 0.1; 0.1];
    
    % Connectivityregions
    agents{i}.polytopes.conn_region.P = [eye(2);-eye(2);[-1 1];[1 1];[1 -1];[-1 -1]];
    agents{i}.polytopes.conn_region.q = [1.0;1.0;1.0;1.0;1.5;1.5;1.5;1.5];
   
end







end

